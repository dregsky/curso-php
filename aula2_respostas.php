<?php
	//Exercício 1:
	$nome=$_POST['nome'];
	$sexo=$_POST['sexo'];
	$cores=$_POST['cor'];
	$tam = count($cores);

	//Exercício 2:

	$name=$_POST['pname'];
	$sname=$_POST['sname'];
	$email=$_POST['email'];
	$sexo2=$_POST['sexo2'];
	$data_nasc=$_POST['data_nascimento'];
	$cargo=$_POST['cargo'];
	$diretoria=$_POST['diretoria'];
	$curso=$_POST['cursoSup'];
	$login=$_POST['login'];
	$senha=$_POST['senha'];

	$arquivo = fopen("saida.txt", "w+");
	fwrite($arquivo, "Nome: $name \n");
	fprintf($arquivo, "Sobrenome: %s\n", $sname);
	fprintf($arquivo, "Email: %s\n", $email);
	fprintf($arquivo, "Sexo: %s\n", $sexo2);
	fprintf($arquivo, "Data de Nascimento: %s\n", $data_nasc);
	fprintf($arquivo, "Cargo: %s\n", $cargo);
	fprintf($arquivo, "Diretoria: %s\n", $diretoria);
	fprintf($arquivo, "Curso: %s\n", $curso);
	fprintf($arquivo, "Login: %s\n", $login);
	fprintf($arquivo, "Senha: %s\n", $senha);
	fclose($arquivo);

	// Exercício 3

	$n1=$_POST['n1'];
	$n2=$_POST['n2'];

	include("divisao.php");

	$resultado = divide($n1,$n2);
?>

<!-- Página HTML -->
<?php include("header.php") ?>
	<div id="content">
		<div id="content_inside">
			<div id="content_inside_header">
				<div class="active" style="margin-left:420px !important;"></div>
			</div><br>
			<div id="content_inside_main">	
				<div>
					<h2>Exercício 1:</h2><br>
					<?php 
						if($nome==NULL) echo 'Não foi entrado com os dados!';
						
						else{
							echo 'O Nome '.': '.'<b>'.$nome.'</b><br> '.'Sexo: '.'<b>'.$sexo.'</b> <br>'.'Cores : ';
							foreach($cores as $i => $cor){ 
								echo '<b>'.$cor.'</b>'; 
								if($i<$tam-1) echo ', '; 
								else echo '.';
							}
						}
					?>
				</div>
				<br>
				<div>
					<h2>Exercício 2:</h2><br>
					<?php 
						if($nome==NULL) echo 'Não foi entrado com os dados!';
						
						else echo 'O resultado foi inserido no arquivo: saida.txt <br>';
							 echo '<b>OBS:</b> Caso o arquivo não esteja sendo criado, verifique a permissão da pasta onde o mesmo está tentando ser criado.';
						
					?>
				</div>
				<br>
				<div>
					<h2>Exercício 3:</h2><br>
					<?php 
						if($n1==NULL) echo 'Não foi entrado com os dados!';
						
						else {
							if($n2!=0) echo 'A divisão '.$n1.' / '.$n2.' = '.$resultado;
							else echo $resultado;
						}
						
					?>
				</div>	
			</div>
		</div>	
	<?php include("inc/footer.php") ?>
	</div>