<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--

	Smooth and Sleek by christopher robinson
	http://www.edg3.co.uk/
	hope you enjoy it and find it usefull :)

-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head profile="http://gmpg.org/xfn/11">
	<title>Curso de PHP-CJR</title>	
	<link rel="shortcut icon" href="image/favicon.ico" />
	<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="en-gb" />
	<meta http-equiv="imagetoolbar" content="false" />
	<meta name="author" content="Christopher Robinson" />
	<meta name="copyright" content="Copyright (c) Christopher Robinson 2005 - 2007" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />	
	<meta name="last-modified" content="Sat, 01 Jan 2007 00:00:00 GMT" />
	<meta name="mssmarttagspreventparsing" content="true" />	
	<meta name="robots" content="index, follow, noarchive" />
	<meta name="revisit-after" content="7 days" />
</head>

<body>
	<div id="header">
		<div id="header_inside">
			<a href="index.php"><h1><span>CURSO</span>PHP</h1></a>
			<ul>
				<li><a href="#">Aula 6</a></li>
				<li><a href="#">Aula 5</a></li>
				<li><a href="#">Aula 4</a></li>
				<li><a href="aula3_respostas.php">Aula 3</a></li>
				<li><a href="aula2.php">Aula 2</a></li>
				<li><a href="aula1.php">Aula 1</a></li>
			</ul>		
		</div>
	</div>