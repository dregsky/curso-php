<?php
	//Exercício 1:
	$dados['salariomin']=$_POST['salariomin'];
	$dados['outrosalario']=$_POST['outrosalario'];

	$quant_salarios= $outrosalario/$salariomin;

	//Exercício 2:
	$nota = array();
	for($i=1;$i<=4;$i++){
		$nota[$i]=$_POST["nota$i"];
	}
	$media = ($nota[1]+$nota[2]+$nota[3]+$nota[4])/4;

	//Exercício 3:

	$sexo=$_POST['sexo'];
	$altura=$_POST['altura'];
	$k=0;
	if($sexo=='Masculino'){
		$pesoideal=(72.7*$altura)-58;
		$k=1;
	}
	else{
		$pesoideal=(62.1*$altura)-44.7;
	}

	//Exercício 4:
	$salario=$_POST['salario'];
	$j=0;

	if($salario<=300){
		$salario=1.5*$salario;
		$j=1;
	}
	else{
		$salario=1.3*$salario;
	}

	//Exercício 5:
	$valormenor=$_POST['valormenor'];
	$valormaior=$_POST['valormaior'];

	//Exercício 6:
	$valor[0]=$_POST['valor1'];
	$valor[1]=$_POST['valor2'];
	$soma = 0;

	//Exercício 7:
	$frase=$_POST['frase'];
	$tam = strlen($frase);

	//Exercício 8:
	$vetor=$_POST['array'];
	
	$vetor = str_split($vetor); // transforma uma string em um array (caracter por caracter)
	$max = max($vetor);
	$indice = array_keys($vetor, $max);

	//Exercício 9:
	$matriz = array(
					array(0,0,0,0,0,0),
					array(0,0,0,0,0,0),	
					array(0,0,0,0,0,0),
					array(0,0,0,0,0,0)
				);
	
	$i=$_POST['linha'];
	$j=$_POST['coluna'];

	$matriz[$i][$j-1] = 1;
	$matriz[$i][$j+1] = 1;
	$matriz[$i+1][$j] = 1;
	$matriz[$i+1][$j+1]= 1;
	$matriz[$i+1][$j-1]= 1;
	$matriz[$i-1][$j]= 1;
	$matriz[$i-1][$j+1]= 1;
	$matriz[$i-1][$j-1]= 1;	
	
	//Exercício 10:
	$x=$_POST['x'];
	$y=$_POST['y'];	
	
?>

<!-- Página HTML -->
<?php include("inc/header.php") ?>
	<div id="content">
		<div id="content_inside">
			<div id="content_inside_header">
				<div class="active" style="margin-left:346px !important;"></div>
			</div><br>
			<div id="content_inside_main">	
				<div>
					<h2>Exercício 1:</h2><br>
					<?php 
						if($salariomin==1) echo 'Não foi entrado com os dados!';
						
						else{
							echo 'O salário'.'  R$'.$outrosalario.' '.'equivale a '.'<b>'.$quant_salarios.'</b>'.' salários mínimos( R$'.$salariomin.').';
						}
					?>
				</div>
				<br>	
				<div>
					<h2>Exercício 2:</h2><br>
					<?php if($media==NULL) echo 'Não foi entrado com os dados!'; else echo "A média é : <b>$media</b>"; 
					?>
				</div>
				<br>	
				<div>
					<h2>Exercício 3:</h2><br>
					<?php 
						if($altura==NULL || $sexo == 'not') echo 'Não foi entrado com os dados!';
						
						else{ 
							echo 'O Peso ideal del'; if($k==0) echo 'a :'; else echo 'e :'; echo '<b>'.$pesoideal.' Kg'.'</b>'; 
						}
					?>
				</div>
				<br>	
				<div>
					<h2>Exercício 4:</h2><br>
					<?php 
						if($salario==NULL) echo 'Não foi entrado com os dados!';
						
						else{ 
							if($j==0) echo 'O reajuste foi de 30%'; else echo 'O reajuste foi de 50%'; echo ', o novo salário é: '.'<b>'.'R$ '.$salario.'</b>'; 
						}
					?>
				</div>
				<br>	
				<div>
					<h2>Exercício 5:</h2><br>
					<?php 
						if($valormaior==NULL) echo 'Não foi entrado com os dados!';
						
						else{ 
							for($i=$valormenor;$i<$valormaior-1;$i++){
								echo ($i+1).' ';
							}
						}
					?>
				</div>
				<br>	
				<div>
					<h2>Exercício 6:</h2><br>
					<?php 
						if($valor[1]==NULL) echo 'Não foi entrado com os dados!';
						
						else{ 
							for($i=0;$i<$valor[1];$i++){
								$soma = $soma + $valor[0];
							}
							echo $valor[0].' x '.$valor[1].' = '.$soma;
						}
					?>
				</div>
				<br>	
				<div>
					<h2>Exercício 7:</h2><br>
					<?php 
					
						if($frase==NULL) echo 'Não foi entrado com os dados!';
						
						else{ 
							for($i=1 ; $i<$tam; $i++){
								echo $i.' ';
							}
							
						}
					?>
				</div>
				<br>	
				<div>
					<h2>Exercício 8:</h2><br>
					<?php 
						if($vetor==NULL) echo 'Não foi entrado com os dados!';
						
						else{ 
								echo 'o maior elemento do array  é: '.$max.' e sua posição é '.$indice[0];
						}
							
						
					?>
				</div>
				<br>	
				<div>
					<h2>Exercício 9:</h2><br>
					<?php 
						if($i==NULL) echo 'Não foi entrado com os dados!';
						
						else{ 
								echo 'a posicao escolhida foi :'.$_POST['linha'].'x'.$_POST['coluna'].'<br>';
								echo '--..'.' 0 1 2 3 4 5'.'<br>';
								for($i=0 ; $i<4 ; $i++){
									echo $i.' [ ';
									for($j=0 ;$j<6; $j++){
										echo $matriz[$i][$j].' ';
									}
									echo ']'.'<br>';
								}
								
						}
							
						
					?>
				</div>
				<br>	
				<div>
					<h2>Exercício 10:</h2><br>
					<?php 
						if($x==NULL) echo 'Não foi entrado com os dados!';
						
						else{ 
							if($y==(($x/3)+1)) echo 'O ponto x = '.$x.' e y = '.$y." <b>pertence</b> a reta";
							else	echo 'O ponto x = '.$x.' e y = '.$y." <b>Não pertence </b> a reta";							
						}
								
					?>
				</div>
			</div>
		</div>	
	<?php include("inc/footer.php") ?>
	</div>